module Types.Game (
  CheckerType,
  Point,
  CheckerFigure,
  CheckerColor,
  Player,
  Step,
  GameState(..),
) where

import Data.UUID (UUID)

data CheckerType = Checker | Queen

data Point = Point
  { x :: Integer
    y :: Integer
  }

data CheckerFigure = CheckerFigure
  { checkerType :: CheckerType,
    point :: Point
  }

data CheckerColor = White | Black

data Player = Player
  { color :: CheckerColor }

data Step = Step
  { figure :: CheckerFigure,
    from :: Point,
    to :: Point
  }

data GameState = GameState
  { board :: [[Maybe CheckerFigure]], -- игровое поле
    players :: [Player]
    turn :: CheckerColor,
    uuid :: UUID
  }